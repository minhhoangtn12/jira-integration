//
//  MemoryGame.swift
//  memorize
//
//  Created by Minh Hoàng Nguyễn Viết  on 26/02/2023.
//

import Foundation

struct MemoryGame<CardContent: Equatable> {
    private(set) var cards: [Card]
    private var faceUpCardIndex: Int? {
        get {
            cards.indices.filter { cards[$0].isFaceUp }.oneAndOnly
        }

        set {
            cards.indices.forEach { cards[$0].isFaceUp = ($0 == newValue) }
        }
    }

    init(cardPairsNumber: Int, createCardContent: (Int) -> CardContent) {
        cards = []
        for pairIndex in 0 ..< cardPairsNumber {
            let content = createCardContent(pairIndex)
            cards.append(Card(content: content, id: pairIndex * 2))
            cards.append(Card(content: content, id: pairIndex * 2 + 1))
        }
    }

    mutating func choose(_ card: Card) {
        guard let chosenIndex = cards.firstIndex(where: { card.id == $0.id }),
              !cards[chosenIndex].isFaceUp,
              !cards[chosenIndex].isMatched
        else {
            return
        }

        if let potentialMatchIndex = faceUpCardIndex {
            if cards[chosenIndex].content == cards[potentialMatchIndex].content {
                cards[chosenIndex].isMatched = true
                cards[potentialMatchIndex].isMatched = true
            }
            cards[chosenIndex].isFaceUp = true
        } else {
            faceUpCardIndex = chosenIndex
        }
    }

    struct Card: Identifiable {
        var isFaceUp = false
        var isMatched = false
        let content: CardContent
        let id: Int
    }
}

extension Array {
    var oneAndOnly: Element? {
        count == 1 ? first : nil
    }
}
