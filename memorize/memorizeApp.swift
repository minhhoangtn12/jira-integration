//
//  memorizeApp.swift
//  memorize
//
//  Created by Minh Hoàng Nguyễn Viết  on 20/02/2023.
//

import SwiftUI

@main
struct memorizeApp: App {
    private let game = EmojiMemoryGame()

    var body: some Scene {
        WindowGroup {
            EmojiMemoryGameView(game: game)
        }
    }
}
