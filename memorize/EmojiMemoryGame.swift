//
//  EmojiMemoryGame.swift
//  memorize
//
//  Created by Minh Hoàng Nguyễn Viết  on 26/02/2023.
//

import SwiftUI


// Why ViewModel need to be a class:
// 1: To conform ObservableObject protocol
// 2: To mutate the model (struct type) without adding mutating keyword
class EmojiMemoryGame: ObservableObject {
    typealias Card = MemoryGame<String>.Card

    private static let emojis = ["🐶", "🐱", "🐭", "🐹", "🐰", "🦊", "🐻", "🐼"]

    //Mark as Published make this auto-notify when ViewModel changed
    @Published private var model = MemoryGame<String>(cardPairsNumber: 4) { index in
        emojis[index]
    }

    var cards: [Card] {
        return model.cards
    }

    // MARK: - Intent(s)

    func choose(card: Card) {
        model.choose(card)
        objectWillChange.send() //Call when we want to notify that ViewModel changed (Imperative way)
    }
}
