//
//  EmojiMemoryGameView.swift
//  memorize
//
//  Created by Minh Hoàng Nguyễn Viết  on 20/02/2023.
//

import SwiftUI

struct EmojiMemoryGameView: View {
    // Mark this to subcribe to this changes
    // Since a view can have multiple view model, we should name a specific name
    @ObservedObject var game: EmojiMemoryGame

    let emojis = ["🐶", "🐱", "🐭", "🐹", "🐰", "🦊", "🐻", "🐼"]
    @State var emojiCount = 4

    var body: some View {
        ScrollView {
            LazyVGrid(columns: [GridItem(.adaptive(minimum: 100))], spacing: 10) {
                ForEach(game.cards, id: \.id) { card in
                    CardView(card: card).aspectRatio(2 / 5, contentMode: .fill)
                        .onTapGesture {
                            game.choose(card: card)
                        }
                }
            }.padding(.horizontal)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        EmojiMemoryGameView(game: EmojiMemoryGame())
    }
}

struct CardView: View {
    let card: EmojiMemoryGame.Card

    var body: some View {
        GeometryReader { geometry in
            ZStack {
                let shape = RoundedRectangle(cornerRadius: DrawingConstant.cornerRadius)
                if card.isFaceUp {
                    shape.fill().foregroundColor(.white)
                    shape.strokeBorder(lineWidth: DrawingConstant.lineWidth)
                    Text(card.content).font(font(geometry.size))
                } else if card.isMatched {
                    shape.opacity(0)
                } else {
                    shape.fill()
                }
            }
            .foregroundColor(.red)
        }
    }

    private func font(_ size: CGSize) -> Font {
        .system(size: min(size.width, size.height) * DrawingConstant.fontScale)
    }

    private enum DrawingConstant {
        static let cornerRadius: CGFloat = 10
        static let lineWidth: CGFloat = 3
        static let fontScale: CGFloat = 0.8
    }
}
